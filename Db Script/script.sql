USE [master]
GO
/****** Object:  Database [LPODatabase]    Script Date: 8/27/2021 5:17:25 PM ******/
CREATE DATABASE [LPODatabase]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LPODatabase', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\LPODatabase.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LPODatabase_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\LPODatabase_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [LPODatabase] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LPODatabase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LPODatabase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LPODatabase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LPODatabase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LPODatabase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LPODatabase] SET ARITHABORT OFF 
GO
ALTER DATABASE [LPODatabase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LPODatabase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LPODatabase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LPODatabase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LPODatabase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LPODatabase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LPODatabase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LPODatabase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LPODatabase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LPODatabase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LPODatabase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LPODatabase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LPODatabase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LPODatabase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LPODatabase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LPODatabase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LPODatabase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LPODatabase] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LPODatabase] SET  MULTI_USER 
GO
ALTER DATABASE [LPODatabase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LPODatabase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LPODatabase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LPODatabase] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LPODatabase] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LPODatabase] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [LPODatabase] SET QUERY_STORE = OFF
GO
USE [LPODatabase]
GO
/****** Object:  Table [dbo].[PoDetails]    Script Date: 8/27/2021 5:17:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PoDetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[f_ld] [int] NULL,
	[order_type] [varchar](50) NULL,
	[delivery_type] [varchar](50) NULL,
	[supplier_code] [varchar](50) NULL,
	[created_date] [varchar](50) NULL,
 CONSTRAINT [PK_PoDetails] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PoMaster]    Script Date: 8/27/2021 5:17:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PoMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[item_name] [varchar](150) NULL,
	[item_price] [int] NULL,
	[item_quantity] [int] NULL,
	[created_date] [varchar](50) NULL,
	[lpo_no] [varchar](50) NULL,
 CONSTRAINT [PK_PoMaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PoDetails] ON 

INSERT [dbo].[PoDetails] ([id], [f_ld], [order_type], [delivery_type], [supplier_code], [created_date]) VALUES (7, 7, N'B', N'Z', N'100', N'8/27/2021 10:42:36 AM')
INSERT [dbo].[PoDetails] ([id], [f_ld], [order_type], [delivery_type], [supplier_code], [created_date]) VALUES (8, 8, N'B', N'Z', N'100', N'8/27/2021 4:28:01 PM')
INSERT [dbo].[PoDetails] ([id], [f_ld], [order_type], [delivery_type], [supplier_code], [created_date]) VALUES (9, 9, N'C', N'X', N'200', N'8/27/2021 4:28:59 PM')
INSERT [dbo].[PoDetails] ([id], [f_ld], [order_type], [delivery_type], [supplier_code], [created_date]) VALUES (10, 10, N'A', N'Y', N'300', N'8/27/2021 4:30:05 PM')
INSERT [dbo].[PoDetails] ([id], [f_ld], [order_type], [delivery_type], [supplier_code], [created_date]) VALUES (11, 11, N'A', N'X', N'100', N'8/27/2021 4:47:22 PM')
INSERT [dbo].[PoDetails] ([id], [f_ld], [order_type], [delivery_type], [supplier_code], [created_date]) VALUES (12, 12, N'A', N'Z', N'200', N'8/27/2021 4:48:10 PM')
SET IDENTITY_INSERT [dbo].[PoDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[PoMaster] ON 

INSERT [dbo].[PoMaster] ([id], [item_name], [item_price], [item_quantity], [created_date], [lpo_no]) VALUES (7, N'aqib ', 34, 4, N'8/27/2021 10:42:36 AM', N'3630')
INSERT [dbo].[PoMaster] ([id], [item_name], [item_price], [item_quantity], [created_date], [lpo_no]) VALUES (8, N'lpg', 2500, 1, N'8/27/2021 4:28:01 PM', N'3134')
INSERT [dbo].[PoMaster] ([id], [item_name], [item_price], [item_quantity], [created_date], [lpo_no]) VALUES (9, N'petrol', 3500, 2, N'8/27/2021 4:28:59 PM', N'3134')
INSERT [dbo].[PoMaster] ([id], [item_name], [item_price], [item_quantity], [created_date], [lpo_no]) VALUES (10, N'cylinder', 4500, 3, N'8/27/2021 4:30:05 PM', N'3134')
INSERT [dbo].[PoMaster] ([id], [item_name], [item_price], [item_quantity], [created_date], [lpo_no]) VALUES (11, N'water containers', 5000, 2, N'8/27/2021 4:47:22 PM', N'3134')
INSERT [dbo].[PoMaster] ([id], [item_name], [item_price], [item_quantity], [created_date], [lpo_no]) VALUES (12, N'Tissues', 500, 6, N'8/27/2021 4:48:10 PM', N'3134')
SET IDENTITY_INSERT [dbo].[PoMaster] OFF
GO
/****** Object:  StoredProcedure [dbo].[deleteRecord]    Script Date: 8/27/2021 5:17:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE PROCEDURE [dbo].[deleteRecord]   
   @id int    
   AS    
BEGIN  
  DELETE FROM PoMaster WHERE PoMaster.id = @id;
  DELETE FROM PoDetails WHERE PoDetails.f_ld  = @id;

END
GO
/****** Object:  StoredProcedure [dbo].[fetch_all_Record]    Script Date: 8/27/2021 5:17:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fetch_all_Record]
AS
BEGIN
    SELECT * 
    FROM PoMaster
END
GO
/****** Object:  StoredProcedure [dbo].[getDetail]    Script Date: 8/27/2021 5:17:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getDetail]  
   @id int  
   AS  
BEGIN  
SELECT *  
FROM PoMaster poM   
left JOIN PoDetails poD on poM.id=poD.f_ld  
where poM.id=@id  
END
GO
USE [master]
GO
ALTER DATABASE [LPODatabase] SET  READ_WRITE 
GO
