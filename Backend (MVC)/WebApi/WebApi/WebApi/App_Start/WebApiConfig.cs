﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
namespace WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var cors = new EnableCorsAttribute("*", "*", "*"); //origin, headers, methods

            config.EnableCors(cors);

            //config.EnableCors();
            config.Formatters.XmlFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("multipart/form-data"));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
//// api/Country/WithStates
//config.Routes.MapHttpRoute(
//  name: "ControllerAndActionOnly",
//  routeTemplate: "api/{controller}/{action}",
//  defaults: new { },
//  constraints: new { action = @"^[a-zA-Z]+([\s][a-zA-Z]+)*$" });

//config.Routes.MapHttpRoute(
//  name: "DefaultActionApi",
//  routeTemplate: "api/{controller}/{action}/{id}",
//  defaults: new { id = RouteParameter.Optional }
//);