﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel;
using WebApi.Models;
using System.Data;
using System.Data.SqlClient;

namespace WebApi.Controllers
{
    public class CrudApiController : ApiController
    {
         LPODatabaseEntities db = new LPODatabaseEntities();

        [HttpGet]
        public object getAll()
        {

            var sp = db.Database.SqlQuery<Order>("fetch_all_Record").ToList<Order>();
            //var list = db.PoMasters.ToList();
            return sp;
        }

        [HttpPost]
        public string addOrder(Order order)
        {
            //db.Database.ExecuteSqlCommand("InsertData @item_name, @item_price, @item_quantity, @lpo_no, @created_date, @order_type, @delivery_type, @supplier_code, @fcreated_date", order.item_name, order.item_price, order.item_quantity, order.lpo_no, DateTime.Now.ToString(), order.order_type, order.delivery_type, order.supplier_code, DateTime.Now.ToString());
            db.insert_method(order.item_name, order.item_price, order.item_quantity, order.lpo_no, DateTime.Now.ToString(), order.f_id, order.order_type, order.delivery_type, order.supplier_code, DateTime.Now.ToString());
            //PoMaster poM = new PoMaster();
            //poM.item_name = order.item_name;
            //poM.item_price = order.item_price;
            //poM.item_quantity = order.item_quantity;
            //poM.lpo_no = order.lpo_no;
            //poM.created_date = DateTime.Now.ToString();
            //db.PoMasters.Add(poM);
            //db.SaveChanges();
            //int lastid = poM.id;
            //PoDetail poD = new PoDetail();
            //poD.f_ld = lastid;
            //poD.order_type = order.order_type;
            //poD.delivery_type = order.delivery_type;
            //poD.supplier_code = order.supplier_code;
            //poD.created_date = DateTime.Now.ToString();
            //db.PoDetails.Add(poD);
            //db.SaveChanges();
            return "Record Inserted";
        }

      
        [HttpGet]
        public object getOrderDetail(int id)
        {
            db.Detail_data(id);
            var sp = db.Database.SqlQuery<Order>("getDetail @id= '" + id + "'").ToList<Order>();

            return sp;
        }
        [HttpPut]
        public object updateOrder(Order order)
        {
            //Get Single course which need to update  
            var PoMrecord = db.PoMasters.Single(x =>x.id == order.id);
            //Field which will be update  
            PoMrecord.item_name = order.item_name;
            PoMrecord.item_price = order.item_price;
            PoMrecord.item_quantity = order.item_quantity;

            db.SaveChanges();
            return "updated";
        }
        [HttpDelete]
        public String DeleteRecord(int id)
        {
            db.Database.ExecuteSqlCommand("exec deleteRecord @id",
                     new SqlParameter("@id", id)
                  );
            return "deleted";
        }
    }
}

public class Order
{
    public int? id  { get; set; }
    public int? f_id { get; set; }
    public string item_name { get; set; }
    public int? item_price { get; set; }
    public int? item_quantity { get; set; }
    public string lpo_no { get; set; }
    public string created_date { get; set; }
    public string order_type { get; set; }
    public string delivery_type { get; set; }
    public string supplier_code { get; set; }
}

//public class getdata
//{
//    public int? id { get; set; }
//    public string item_name { get; set; }
//    public int? item_price { get; set; }
//    public int? item_quantity { get; set; }
//    public string lpo_no { get; set; }
//    public string created_date { get; set; }
//}

