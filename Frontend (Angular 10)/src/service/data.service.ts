import { APP_ID, Injectable } from '@angular/core';
import { HttpClient ,HttpResponse ,HttpHeaders,HttpParams} from '@angular/common/http';
import { global_pointer } from '../../src/assets/js/global_config';
import { async } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// import { GlobalService } from '../../src/service/global.service'

@Injectable({
  providedIn: 'root'
})
export class DataService {
 
 
  constructor(private http: HttpClient,private router: Router,private toastr: ToastrService) { 
    
  }
   

  //Public Ip
  ip :string=global_pointer.publicip;

   header:any = new HttpHeaders().set(
    "Content-Type", "application/json",
  ).set('Content-Length', '140317').set('Access-Control-Allow-Origin','*').set('Host', 'https://localhost:44348');


 
  createOrder(data) {
    return this.http.post("https://localhost:44348/api/CrudApi/addOrder",data,{headers:this.header})
  }
  getAllRecords(){
    return this.http.get("https://localhost:44348/api/CrudApi/getAll")
  }
  deleteRecord(id){
    return this.http.delete("https://localhost:44348/api/CrudApi/DeleteRecord",{params:id})
  }
  viewDetail(id){
    return this.http.get("https://localhost:44348/api/CrudApi/getOrderDetail",{params:id})
  }

  updateOrder(data){
    return this.http.put("https://localhost:44348/api/CrudApi/updateOrder",data,{headers:this.header})
  }
 
}
