import { Component, OnInit,ViewChild, ElementRef  } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { faTrash, faEye, faEdit } from '@fortawesome/free-solid-svg-icons';
// import * as $ from 'jquery';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { setStyle } from '@amcharts/amcharts4/.internal/core/utils/DOM';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DataService } from 'src/service/data.service';
import { HttpParams } from '@angular/common/http';
import  alasql  from 'alasql';
declare var $:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  orderForm: FormGroup;
  
  result: any;
  body = true;
  faTrash = faTrash;
  faEye=faEye;
  faEdit=faEdit;
  section = false;
  allRecordList=[];
  viewRecordList=[];
  submitted = false;
  createMode=false;
  updatejson={};
  current_user_id:any;
  constructor(private formBuilder: FormBuilder, private api: DataService, private ngxService: NgxUiLoaderService, private toastr: ToastrService, private router: Router) {
    this.createOrderForm();
  }


  ngOnInit(): void {
    this.getAllRecords();
    this.ngxService.start();
    setTimeout(() => {
      this.ngxService.stop();
    }, 1000)


  }
  get f() { return this.orderForm.controls; }

  //initialize user form
  createOrderForm() {
    this.orderForm = this.formBuilder.group({
      item_name: ['', Validators.required],
      item_price: ['', Validators.required],
      item_quantity: ['', Validators.required],
      lpo_no: [this.getRandomInt(), Validators.required],
      order_type: ['', Validators.required],
      delivery_type: ['', Validators.required],
      supplier_code: ['', Validators.required],
    });
  }

  enableCreateMode(){
    this.createMode=true;
  }
  DisableMode(){
    this.createMode=false;
  }
  createOrder() {
    this.ngxService.start();
    this.submitted = true;
    if (this.orderForm.invalid) {
      return;
    }

    var json={
      item_name:this.orderForm.value.item_name,
      item_price:this.orderForm.value.item_price,
      item_quantity:this.orderForm.value.item_quantity,
      lpo_no: this.getRandomInt(),
      order_type: this.orderForm.value.order_type,
      delivery_type: this.orderForm.value.delivery_type,
      supplier_code: this.orderForm.value.supplier_code
    }
    this.api.createOrder(JSON.stringify(json)).subscribe((data: any) => {
      if (data=="Record Inserted") {
        this.toastr.info("Record Inserted");
        this.createMode=false;
        
        this.getAllRecords();
        
      }
      else {
        this.toastr.error("Data not Added");
      }
      this.ngxService.stop();
    }
    );


  }
  fetchOrder(data) {
     this.updatejson="";
    this.orderForm.controls['item_name'].setValue(data.item_name);
    this.orderForm.controls['item_price'].setValue(data.item_price);
    this.orderForm.controls['item_quantity'].setValue(data.item_quantity);
    this.orderForm.controls['lpo_no'].setValue(data.lpo_no);
    this.current_user_id=data.id;

  }
  
  updateOrder(data) {
    
    this.updatejson={
      id:this.current_user_id,
      item_name:this.orderForm.value.item_name,
      item_price:this.orderForm.value.item_price,
      item_quantity:this.orderForm.value.item_quantity,
    }
    this.ngxService.start();
    this.api.updateOrder(JSON.stringify(this.updatejson)).subscribe((data: any) => {
      if (data) {
        this.toastr.info("Record updated");
        this.createMode=false;
        $("#updateModal").modal("hide");
        this.getAllRecords();
        
      }
      else {
        this.toastr.error("Data not update");
      }
      this.ngxService.stop();
    }
    );


  }
  viewDetail(id) {

    this.ngxService.start();
    let params = new HttpParams().set("id", id)
    this.api.viewDetail(params).subscribe((data: any) => {



      if (data) {
        this.viewRecordList=[];
        var response=data[0];
        
          var res = {
            item_name: response.item_name || "N/A",
            delivery_type: response.delivery_type || "N/A",
            order_type: response.order_type || "N/A",
            lpo_no: response.lpo_no || "N/A",
          }
          this.viewRecordList.push(res);
        
      }
      else {
        this.toastr.error("Error");
      }
      this.ngxService.stop();
    },
      (error) => {
        console.log('error from service', error);
      }
    )

  }
  deleteRecord(id) {

    this.ngxService.start();
    let params = new HttpParams().set("id", id)
    this.api.deleteRecord(params).subscribe((data: any) => {



      if (data) {

        this.getAllRecords();
      }
      else {
        this.toastr.error("error");
      }
      this.ngxService.stop();
    },
      (error) => {
        console.log('error from service', error);
      }
    )

  }



  getAllRecords() {

    this.ngxService.start();

    this.api.getAllRecords().subscribe((data: any) => {



      if (data) {

        this.allRecordList=[];
        for (var i = 0; i < data.length; i++) {
          var res = {
           id: data[i].id || 0,
           item_name: data[i].item_name || "N/A", 
           item_price: data[i].item_price || "N/A", 
           item_quantity: data[i].item_quantity || "N/A", 
           lpo_no: data[i].lpo_no || "N/A", 
           created_date: data[i].created_date || "N/A" 
 
          }
          this.allRecordList.push(res);
        }
      }
      else {
        this.toastr.error(data.message);
      }
      this.ngxService.stop();
    },
      (error) => {
        console.log('error from service', error);
      }
    )

  }





  saveFile() {
    
    var opts = [{ sheetid: 'One', header: true }, { sheetid: 'Two', header: false }];
    var res = alasql('SELECT * INTO XLSX("Records.xlsx",?) FROM ?',
      [opts, [this.allRecordList]]);
  }

  getRandomInt() {
    var min=1000;
    var max=5000
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

    
}
