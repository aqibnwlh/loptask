import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }
  
  userdata: any;
  fullname:string;

  ngOnInit(): void {
    
    this.userdata = localStorage.getItem("userinfo");
    if(this.userdata){
      this.fullname= this.userdata;
    }
  }
  
  logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("userinfo");
    localStorage.removeItem("level_type");
    this.fullname="";
    this.router.navigate(['login']);
    
  }
  // Responsive NavBar Start
  NavMenu() 
  {
  document.addEventListener("DOMContentLoaded", function () {
    // make it as accordion for smaller screens
    if (window.innerWidth > 992) {

      document.querySelectorAll('.navbar .nav-item').forEach(function (everyitem) {

        everyitem.addEventListener('mouseover', function (e) {

          let el_link = this.querySelector('a[data-bs-toggle]');

          if (el_link != null) {
            let nextEl = el_link.nextElementSibling;
            el_link.classList.add('show');
            nextEl.classList.add('show');
          }

        });
        everyitem.addEventListener('mouseleave', function (e) {
          let el_link = this.querySelector('a[data-bs-toggle]');

          if (el_link != null) {
            let nextEl = el_link.nextElementSibling;
            el_link.classList.remove('show');
            nextEl.classList.remove('show');
          }


        })
      });

    }
   
  });

  }

  
}
