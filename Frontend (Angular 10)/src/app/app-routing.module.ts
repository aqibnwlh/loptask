import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
import { AuthGuardGuard } from 'src/authGuard/auth-guard.guard';
import { CommonModule } from "@angular/common";
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    // canActivate: [AuthGuardGuard]
  },
  {
    path: '**',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [CommonModule,RouterModule.forRoot(routes, { enableTracing: false })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
